package test;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import runner.Points;
import runner.PointsCounterExtension;
import servlet.MyServlet;
import util.ServletTester;
import util.ServletTester.Response;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@ExtendWith(PointsCounterExtension.class)
public class ServletTest {

    ServletTester tester = new ServletTester(List.of(MyServlet.class));

    @Test
    @Points(10)
    public void canHandleInputJson() {

        String json = """
           {"key1":{"key2":[1,5,7]}}""";

        Response response = tester.makePostRequest("/transform", json);

        String actual = removeWs(response.content());

        assertThat(actual, equalTo("13"));
    }

    @Test
    @Points(5)
    public void canConstructOutputJson() {

        String json = """
           {"key1":{"key2":[1,5,7]}}""";

        Response response = tester.makePutRequest("/transform", json);

        String expected = """
                {"result":[1,5,7]}""";

        String actual = removeWs(response.content());

        assertThat(actual, equalTo(expected));
    }

    private String removeWs(String input) {
        return input.replaceAll("[\n\r ]", "");
    }
}