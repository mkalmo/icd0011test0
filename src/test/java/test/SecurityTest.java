package test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import runner.NoPointsIfThisTestFails;
import runner.Points;
import runner.PointsCounterExtension;
import security.SecurityConfig;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(PointsCounterExtension.class)
@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = { SecurityConfig.class })
public class SecurityTest {

    private WebApplicationContext wac;

    @Autowired
    public void setWac(WebApplicationContext wac) {
        this.wac = wac;
    }

    private MockMvc mvc;

    @BeforeEach
    public void setUp() {
        mvc = MockMvcBuilders
                .webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }

    @Test
    @Points(5)
    public void adminUrlsAreRestricted() throws Exception {

        mvc.perform(get("/admin/test"))
                .andExpect(this::isBadRequest);
    }

    @Test
    @Points(19)
    public void canLogInWithCorrectCredentials() throws Exception {

        mvc.perform(post("/login")
                .contentType(MediaType.TEXT_PLAIN).content("user=alice&pass=s3cr3t"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @Points(6)
    public void canNotLogInWithGetRequest() throws Exception {

        mvc.perform(get("/login")
                .contentType(MediaType.TEXT_PLAIN).content("user=alice&pass=s3cr3t"))
                .andExpect(this::isRestricted);
    }

    @Test
    @NoPointsIfThisTestFails
    public void canNotLogInWithWrongPassword() throws Exception {

        mvc.perform(post("/login")
                        .contentType(MediaType.TEXT_PLAIN).content("user=alice&pass=wrong"))
                .andExpect(this::isRestricted);
    }

    @Test
    @NoPointsIfThisTestFails
    public void canNotLogInWithWrongUser() throws Exception {

        mvc.perform(post("/login")
                        .contentType(MediaType.TEXT_PLAIN).content("user=bob&pass=s3cr3t"))
                .andExpect(this::isRestricted);
    }

    private void isRestricted(MvcResult result) {
        assertThat(result.getResponse().getStatus(),
                    anyOf(is(401), is(403)));
    }

    private void isBadRequest(MvcResult result) {
        assertThat(result.getResponse().getStatus(), is(400));
    }

}