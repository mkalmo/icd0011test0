package runner;

import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

public class Runner {

    public static void main(String[] args) {
        new Runner().run(args[0]);
    }

    public void run(String tag) {
        LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
                .selectors(DiscoverySelectors.selectClass(resolveClass(tag)))
                .build();

        Launcher launcher = LauncherFactory.create();

        PrintStream out = System.out;

        System.setOut(new PrintStream(OutputStream.nullOutputStream()));

        launcher.execute(request);

        out.printf("%s of %s points%n",
                PointsCounterExtension.getPoints(), PointsCounterExtension.getTotalPoints());
    }

    private Class<?> resolveClass(String tag) {
        Map<String, String> map = new HashMap<>();
        map.put("ex1", "test.ServletTest");
        map.put("ex2", "test.SecurityTest");

        if (!map.containsKey(tag)) {
            throw new IllegalStateException("unknown tag: " + tag);
        }

        return loadClass(map.get(tag));
    }

    private Class<?> loadClass(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
