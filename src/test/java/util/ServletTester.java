package util;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletConfig;
import org.springframework.mock.web.MockServletContext;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ServletTester {

    private final List<? extends Class<? extends HttpServlet>> servletClasses;

    private final MockServletContext servletContext = new MockServletContext();
    private final MockServletConfig servletConfig = new MockServletConfig(servletContext);

    public <T extends Class<? extends HttpServlet>> ServletTester(List<T> servletClasses) {
        this.servletClasses = servletClasses;
    }

    public Response makeGetRequest(String path) {
        return makeRequest(HttpMethod.GET, path, null);
    }

    public Response makePostRequest(String path, String data) {
        return makeRequest(HttpMethod.POST, path, data);
    }

    public Response makePutRequest(String path, String data) {
        return makeRequest(HttpMethod.PUT, path, data);
    }

    public Response makePutRequest(String path) {
        return makeRequest(HttpMethod.PUT, path, null);
    }

    public Response makeRequest(HttpMethod httpMethod, String pathWithParams, String data) {
        try {
            return makeRequestSub(httpMethod, pathWithParams, data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Response makeRequestSub(HttpMethod httpMethod,
                                   String pathWithParams, String data) throws Exception {
        MockHttpServletRequest request = createRequest(pathWithParams);

        if (data != null) {
            request.setContent(data.getBytes(StandardCharsets.UTF_8));
        }

        String uri = request.getRequestURI();

        HttpServlet servlet = findServletByPath(uri);

        servlet.init(servletConfig);

        Class<? extends HttpServlet> aClass = servlet.getClass();

        String methodName = switch (httpMethod) {
            case GET -> "doGet";
            case POST -> "doPost";
            case PUT -> "doPut";
            case DELETE -> "doDelete";
        };

        Method doGet = aClass.getDeclaredMethod(methodName,
                HttpServletRequest.class, HttpServletResponse.class);

        MockHttpServletResponse response = new MockHttpServletResponse();

        doGet.invoke(servlet, request, response);

        return new Response(response.getStatus(),
                            response.getContentAsString());
    }

    private HttpServlet findServletByPath(String path) {
        for (Class<? extends HttpServlet> aClass : servletClasses) {

            WebServlet annotation = aClass.getAnnotation(WebServlet.class);

            if (annotation != null && annotation.value()[0].equals(path)) {
                return createServletInstance(aClass);
            }
        }

        throw new IllegalStateException("Servlet not found");
    }

    private static HttpServlet createServletInstance(Class<? extends HttpServlet> aClass) {
        try {
            return (HttpServlet) aClass.getDeclaredConstructors()[0]
                    .newInstance(new Object[]{});
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private MockHttpServletRequest createRequest(String pathWithParams) {
        var request = new MockHttpServletRequest(servletContext);

        UriComponents uriComponents = UriComponentsBuilder
                .fromUriString(pathWithParams)
                .build();

        request.setRequestURI(uriComponents.getPath());
        request.setQueryString(uriComponents.getQuery());
        uriComponents.getQueryParams().forEach(
                (key, values) -> request.addParameter(key, values.toArray(new String[0])));

        return request;
    }

    public enum HttpMethod {
        GET, POST, PUT, DELETE
    }

    public record Response(int statusCode, String content) {}

}
